package input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

public class InputHandler implements KeyListener {
	
	// Event Queue 
	private ArrayList<InputInfo> releaseQueue = new ArrayList<InputInfo>();
	
	// List of all keys
	private ArrayList<InputKey> keyList = new ArrayList<InputKey>();
	
	public InputHandler() {
		keyList.add(new InputKey(38, InputInfo.UARROW_PRESSED, InputInfo.UARROW_RELEASED));
		keyList.add(new InputKey(40, InputInfo.DARROW_PRESSED, InputInfo.DARROW_RELEASED));
		keyList.add(new InputKey(39, InputInfo.RARROW_PRESSED, InputInfo.RARROW_RELEASED));
		keyList.add(new InputKey(37, InputInfo.LARROW_PRESSED, InputInfo.LARROW_RELEASED));
		keyList.add(new InputKey(32, InputInfo.SPACE_PRESSED, InputInfo.SPACE_RELEASED));
		keyList.add(new InputKey(10, InputInfo.ENTER_PRESSED, InputInfo.ENTER_RELEASED));
	}
	
	// Getter info
	public InputInfo handleInput() {	
		if (releaseQueue.size() > 0) {
			return releaseQueue.remove(0);
		}
		return null;
	}
	
	/**
	 * Key Handlers
	 */
	@Override
	public void keyPressed(KeyEvent k) {		
		int keyCode = k.getKeyCode();
		for(InputKey key : keyList) {
			InputInfo result = key.applyKeyPress(keyCode);
			if (result != null) releaseQueue.add(0, result);
		}
	}

	@Override
	public void keyReleased(KeyEvent k) {
		int keyCode = k.getKeyCode();
		for(InputKey key : keyList) {
			InputInfo result = key.applyKeyRelease(keyCode);
			if (result != null) releaseQueue.add(0, result);
		}
	}

	@Override
	public void keyTyped(KeyEvent arg0) {}			
}
