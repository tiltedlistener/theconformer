package entities;

import game.Game;
import game.Vector2D;
import input.InputInfo;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;

import javax.swing.JFrame;

import entities.states.*;

public class Head extends Segment {

	private HeadState currentState;
	private Segment tail;

	public Head(Graphics2D _g, JFrame _frame) {
		super(_g, _frame);
		currentState = new RightState();
	}
	
	public void update(InputInfo input) {
		HeadState newState = (HeadState) currentState.handleInput(this, input);
		if (newState != currentState) { 
			currentState.exit(this);
			currentState = newState;
			currentState.enter(this);
		}
		currentState.update(this);
		
		if (hasNext()) {
			nextSegment.update(getLastPos());
		}	
	}
	
	public void addSegment() {
		Segment seg = new Segment(g2d, frame);
		if (tail == null) {
			Vector2D posClone = getLastPos().clone();
			seg.setPos(posClone);
			setNextSegment(seg);	
		} else {
			Vector2D posClone = tail.getLastPos().clone();
			seg.setPos(posClone);
			tail.setNextSegment(seg);
		}
		tail = seg;
	}
	
	public void clearSegments() {
		if (hasNext()) {
			nextSegment.clearSegment();
			nextSegment = null;
		}
	}
	
	public boolean checkForCollisions() {
		if (hasNext()) {
			return nextSegment.checkForCollision(getPos());
		}
		return false;
	}
	
	public ArrayList<Vector2D> generateVectorList() {
		ArrayList<Vector2D> list = new ArrayList<Vector2D>();
		list.add(getPos());
		if (hasNext()) {
			list = nextSegment.vectorList(list);
		}
		return list;
	}
	
}
