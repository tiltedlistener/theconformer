package entities;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;

import javax.swing.JFrame;

import game.Vector2D;

public class Segment {
	
	// Graphical contexts
	protected JFrame frame;
	protected Graphics2D g2d;
	
	// Positions
	protected Vector2D pos;
	protected Vector2D lastPos;
	protected int width = 20;
	protected int height = 20;
	
	// List Methods
	protected Segment nextSegment = null;
	
	public Segment(Graphics2D _g, JFrame _frame) {
		frame = _frame;
		g2d = _g;
	}
	
	// Vector methods
	public void setPos(Vector2D _pos) {
		lastPos = pos;
		pos = _pos;
		
		if (lastPos == null) {
			lastPos = pos;
		}
	}
	
	public double X() {
		return pos.X();
	}

	public double Y() {
		return pos.Y();
	}	
	
	public int width() {
		return width;
	}
	
	public int height() {
		return height;
	}
	
	public Vector2D getPos() {
		return pos;
	}
	
	public Vector2D getLastPos() {
		return lastPos;
	}
	
	// Loop
	public void draw() {
		g2d.setColor(Color.WHITE);
		g2d.fillRect((int)pos.X(), (int)pos.Y(), width, height);
		if (hasNext()) nextSegment.draw();
	}
	
	public void update(Vector2D _nextPos) {
		setPos(_nextPos);
		if (hasNext()) {
			nextSegment.update(getLastPos());
		}	
	}
	
	// List Methods
	public void setNextSegment(Segment next) {
		nextSegment = next;
	}
	
	public boolean hasNext() {
		if (nextSegment != null) {
			return true;
		}
		return false;
	}
	
	public boolean checkForCollision(Vector2D headPos) {
		if (pos.overlap(headPos)) {
			return true;
		} else {
			if (hasNext()) {
				return nextSegment.checkForCollision(headPos);
			} else {
				return false;
			}
		}
	}
	
	public ArrayList<Vector2D> vectorList(ArrayList<Vector2D> list) {
		list.add(getPos());
		if (hasNext()) {
			return nextSegment.vectorList(list);
		}
		return list;
	}
	
	public void clearSegment() {
		if (hasNext()) {
			nextSegment.clearSegment();
		}
		nextSegment = null;
	}
}
