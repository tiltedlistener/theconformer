package entities;

import game.Game;
import game.Vector2D;

import java.awt.Graphics2D;
import java.util.ArrayList;

import javax.swing.JFrame;

public class PowerUpGenerator {
	
	protected JFrame frame;
	protected Graphics2D g2d;
	protected Head headRef;
	
	public PowerUpGenerator(Graphics2D _g, JFrame _frame, Head _ref) {
		frame = _frame;
		g2d = _g;		
		headRef = _ref;
	}
	
	public PowerUp createPowerUp() {
		PowerUp current = new PowerUp(g2d, frame);
		current.setPos(produceVector());
		return current;
	}
	
	public Vector2D produceVector() {
		int xRandom = 0;
		int yRandom = 0;
		boolean findingRandoms = true;
		ArrayList<Vector2D> vecList = headRef.generateVectorList();
		Vector2D finalVector = new Vector2D();
		
		while (findingRandoms) {
			xRandom = (int)(Math.random() * (Game.rightBounds - Game.leftBounds));
			yRandom = (int)(Math.random() * (Game.lowerBounds - Game.topBounds));
			
			int xMod = xRandom % 20;
			int yMod = yRandom % 20;
			xRandom -= xMod;
			yRandom -= yMod;		
			
			finalVector.setValues(xRandom + Game.leftBounds, yRandom + Game.topBounds);
			for (int i = 0; i < vecList.size(); i++) {
				Vector2D current = vecList.get(i);
				if (current.overlap(finalVector)) {
					break;
				}
				if (vecList.size() - 1 == i) {
					findingRandoms = false;
				}
			}
		}
		return finalVector;
	}

}
