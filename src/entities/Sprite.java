package entities;

import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.JFrame;

import game.Vector2D;

public class Sprite {

	// State
	protected Vector2D pos;
	protected Vector2D vel;
	protected boolean alive = true;
	
	// Graphical contexts
	protected JFrame frame;
	protected Graphics2D g2d;
	protected Image image;
	
	public Sprite(Graphics2D _g, JFrame _frame) {
		frame = _frame;
		g2d = _g;
	}	
	
	public void setImage(Image _img) {
		image = _img;
	}
	
	public void setPos(Vector2D _vec) {
		pos = _vec;
	}
	
	public Vector2D getPos() {
		return pos;
	}
	
	public boolean isAlive() {
		return alive;
	}
	
	public void kill() {
		alive = false;
	}
	
	public void update() {}
	
	public void draw() {
		g2d.drawImage(image, (int)pos.X(), (int)pos.Y(), image.getWidth(frame), image.getHeight(frame), frame);
	}
	
}
