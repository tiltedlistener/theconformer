package entities;

import game.ImageLoader;
import java.awt.Graphics2D;
import javax.swing.JFrame;

public class PowerUp extends Sprite {

	public PowerUp(Graphics2D _g, JFrame _frame) {
		super(_g, _frame);
		setImage(ImageLoader.loadImage("images/unique.png"));
	}

}
