package entities.states;

import game.Vector2D;
import input.InputInfo;
import entities.Head;

public class DownState extends HeadState {

	@Override
	public HeadState handleInput(Head head, InputInfo input) {
		if (input == InputInfo.LARROW_PRESSED) return new LeftState();
		else if (input == InputInfo.RARROW_PRESSED) return new RightState();	
		
		if (!head.hasNext()) {
			if (input == InputInfo.UARROW_PRESSED) return new UpState();
		}
		
		if (input == InputInfo.SPACE_RELEASED) {
			head.addSegment();
		}
		
		return this;
	}

	@Override
	public void update(Head head) {
		Vector2D pos = head.getPos().clone();
		pos.add(new Vector2D(0, 20));
		head.setPos(pos);
	}

	@Override
	public void enter(Head head) {
		// TODO Auto-generated method stub

	}

	@Override
	public HeadState exit(Head head) {
		// TODO Auto-generated method stub
		return null;
	}

}
