package entities.states;

import input.InputInfo;
import entities.Head;

public abstract class HeadState {
	public abstract HeadState handleInput(Head head, InputInfo input);
	public abstract void update(Head head);
	public abstract void enter(Head head);
	public abstract HeadState exit(Head head);
}
