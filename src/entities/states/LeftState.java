package entities.states;

import game.Vector2D;
import input.InputInfo;
import entities.Head;

public class LeftState extends HeadState {

	@Override
	public HeadState handleInput(Head head, InputInfo input) {
		if (input == InputInfo.UARROW_PRESSED) return new UpState();
		else if (input == InputInfo.DARROW_PRESSED) return new DownState();	
		
		if (!head.hasNext()) {
			if (input == InputInfo.RARROW_PRESSED) return new RightState();
		}		
		
		return this;
	}

	@Override
	public void update(Head head) {
		Vector2D pos = head.getPos().clone();
		pos.add(new Vector2D(-20, 0));
		head.setPos(pos);
	}

	@Override
	public void enter(Head head) {
		// TODO Auto-generated method stub

	}

	@Override
	public HeadState exit(Head head) {
		// TODO Auto-generated method stub
		return null;
	}

}
