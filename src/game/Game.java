package game;


import input.InputHandler;
import input.InputInfo;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.ListIterator;

import javax.swing.JFrame;

import entities.*;

public class Game extends JFrame implements Runnable {

	// Settings
	private static final long serialVersionUID = 1L;
	private static String title = "The Conformer";
	private int screenWidth = 800;
	private int screenHeight = 790;
	
	// TODO just use a panel
	public static int leftBounds = 10;
	public static int rightBounds = 790;
	public static int topBounds = 200;
	public static int lowerBounds = 780;
	
	// Loop
	private Thread gameLoop;
	private double previous = System.currentTimeMillis();
	private double lag = 0;
	private double MS_PER_UPDATE = 150;	
	
	// Controls
	private InputHandler input = new InputHandler();
	
	// Graphics
	protected Graphics2D g2d;
	public Graphics2D graphics() { return g2d; }
	protected BufferedImage backBuffer;		
	
	// Sprites
	Head head;
	LinkedList<Sprite> sprites = new LinkedList<Sprite>();
	PowerUpGenerator generator;
	
	// State
	GameState currentState = GameState.START;
	Image startScreen;
	Image endScreen;
	Image backgroundScreen;

	public static void main(String[] args) {
		new Game();
	}
	
	public Game() {
		super(title);
		
		// Screen
		setSize(screenWidth, screenHeight);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);		
		
		// Graphics
		backBuffer = new BufferedImage(screenWidth, screenHeight, BufferedImage.TYPE_INT_RGB);
		g2d = backBuffer.createGraphics();		
		
		// Start Screen
		startScreen = ImageLoader.loadImage("images/title-screen.png");
		endScreen = ImageLoader.loadImage("images/end-screen.png");
		backgroundScreen = ImageLoader.loadImage("images/background.jpg");
		
		// Input
		addKeyListener(input);
		
		// Threads
		gameLoop = new Thread(this);
		gameLoop.start();
		
		// Sprites
		head = new Head(graphics(), this);
		head.setPos(new Vector2D(leftBounds, topBounds));
		
		generator = new PowerUpGenerator(graphics(), this, head);
		sprites.add(generator.createPowerUp());
		
		// Begin
		setVisible(true);
	}

	/**
	 * Game Loop
	 */	
	@Override
	public void run() {
		Thread t = Thread.currentThread();
		while(t == gameLoop) {
			double current = System.currentTimeMillis();
			double elapsed = current - previous;
			previous = current;
			lag += elapsed;
			
			while (lag >= MS_PER_UPDATE) {
				update();
				lag -= MS_PER_UPDATE;
			}	
			repaint();
		}				
	}

	public void stop() {
		gameLoop = null;
	}	

	// JFrame repaint method
	public void paint(Graphics g) {
		g2d.clearRect(0, 0, screenWidth, screenHeight);	
		render();
		g.drawImage(backBuffer, 0, 0, this);
	}	
	
	public void menuScreenUpdate() {
		InputInfo currentCode = input.handleInput();	
		if (currentState == GameState.START) {
			if (currentCode == InputInfo.ENTER_RELEASED) {
				currentState = GameState.PLAY;
			}
		} else if (currentState == GameState.END) {
			if (currentCode == InputInfo.ENTER_RELEASED) {
				currentState = GameState.START;
			}			
		}
	}
	
	public void render() {
		if (currentState == GameState.PLAY) {
			gameDraw();
		} else if (currentState == GameState.START) {
			startScreenDraw();
		} else if (currentState == GameState.END) {
			endScreenDraw();
		}
	}	public void startScreenDraw() {
		g2d.drawImage(startScreen, 0, 0, startScreen.getWidth(this), startScreen.getHeight(this), this);
	}
	
	public void endScreenDraw() {
		g2d.drawImage(endScreen, 0, 0, startScreen.getWidth(this), startScreen.getHeight(this), this);		
	}
	
	public void gameDraw() {
		// Draw Bounds
		g2d.drawImage(backgroundScreen, 0, 0, startScreen.getWidth(this), startScreen.getHeight(this), this);	
		g2d.setColor(Color.BLACK);
		
		g2d.fillRect(leftBounds, topBounds, screenWidth - 20, lowerBounds - 200);
		g2d.setColor(Color.WHITE);
		g2d.drawRect(leftBounds - 1, topBounds - 1, screenWidth - 20, lowerBounds - 200);
		
		// Draw Snake
		head.draw();
		
		// Draw Sprites
		ListIterator<Sprite> listIterator = sprites.listIterator();
        while (listIterator.hasNext()) {
			Sprite current = listIterator.next();
			if (current.isAlive()) {
				current.draw();
			}
        }			
	}
	
	// Update
	public void update() {
		if (currentState == GameState.PLAY) {
			gameUpdate();
		} else if (currentState == GameState.START || currentState == GameState.END) {
			menuScreenUpdate();
		}
	}
	
	public void gameUpdate() {
		spriteCleanUp();		
		
		InputInfo currentInput = input.handleInput();
		head.update(currentInput);
		
		// Only concerns hitting itself, not other objects
		if (head.checkForCollisions() || checkBounds()) {
			endGame();
		}

		// Concerns hitting other in game objects
		checkSpriteCollisions();		
	}	
	
	public boolean checkBounds() {
		if (head.X() + head.width() > Game.rightBounds || head.X() < Game.leftBounds || head.Y() + head.height() > Game.lowerBounds || head.Y() < Game.topBounds) return true;
		return false;
	}
	
	public void checkSpriteCollisions() {
		ListIterator<Sprite> listIterator = sprites.listIterator();
		Vector2D headVec = head.getPos();

        while (listIterator.hasNext()) {
			Sprite current = listIterator.next();
			if (current.getPos().overlap(headVec)) {
				head.addSegment();
				current.kill();
			}
        }
	}
	
	public void spriteCleanUp() {
		for (int i = 0; i < sprites.size(); i++) {
			if (!sprites.get(i).isAlive()) {
				sprites.remove(i);
				sprites.add(generator.createPowerUp());
			}
		}
	}
	
	public void endGame() {
		gameReset();
		currentState = GameState.END;
	}
	
	public void gameReset() {
		head.clearSegments();
		head = new Head(graphics(), this);
		head.setPos(new Vector2D(leftBounds, topBounds));
	}
}
