# The Conformer #

### Intent ###

Simple "Snake" game to demonstrate for students generating your own linked lists and using recursive methods to update the main Snake object. 

Finite state machines and Game state using Enums are also demonstrated. 